package com.carsa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RumboAbmRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(RumboAbmRestApplication.class, args);
	}

}
