package com.carsa.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.carsa.model.Vendedor;
import com.carsa.service.VendedorService;


@RestController
@RequestMapping("/vendedores")
public class VendedorController {
	
	@Autowired
	VendedorService vendedorService;
	
	@GetMapping("/nombre/{nombre}")
	public ResponseEntity<List<Vendedor>> getVendedoresByNombre(@PathVariable(name = "nombre") String nombre )
	{
		List<Vendedor> response = vendedorService.findAllByNombre(nombre);
		return new ResponseEntity<> (response,  HttpStatus.OK);
	}
	
	@PutMapping
	public Vendedor saveVendedor(@RequestBody Vendedor vendedor) {
		return vendedorService.save(vendedor);
	}
	
	@PostMapping
	public boolean addVendedor(@RequestBody Vendedor vendedor) throws Exception {
		if(vendedor.getIdVendedor()!=null) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "La entidad a guardar no puede tener id");
		}
		return vendedorService.add(vendedor);
	}
	
	@DeleteMapping("/{id}")
	public boolean borrarVendedor(@PathVariable long id) throws Exception {
		return vendedorService.delete(id);	
	}

}
