package com.carsa.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import com.carsa.model.Marca;

@Entity
@Table(name = "ARTICULO")
@PrimaryKeyJoinColumn(name = "ID_PRODUCTO")
public class Articulo extends Producto{
	
	@OneToOne
	@JoinColumn(name = "ID_MARCA", referencedColumnName = "ID_MARCA")
	private Marca marca;
	
	@Column(name = "ID_PROVEEDOR")
	private Long idProveedor;

	public Marca getMarca() {
		return marca;
	}

	public void setMarca(Marca marca) {
		this.marca = marca;
	}

	public Long getIdProveedor() {
		return idProveedor;
	}

	public void setIdProveedor(Long idProveedor) {
		this.idProveedor = idProveedor;
	}
	
}