package com.carsa.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity
@Table(name = "RUBRO")
@Inheritance(strategy = InheritanceType.JOINED) 
public class Rubro {
	@Id
	@Column(name = "ID_RUBRO")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "RUBRO_ID")
	private String rubroID;
	
	@Column(name = "DESCRIPCION")
	private String descripcion;
	
	@Column(name = "CATEGORIA_WEB_CMCOM")
	private String categoriaWebCmcom;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRubroID() {
		return rubroID;
	}

	public void setRubroID(String rubroID) {
		this.rubroID = rubroID;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getCategoriaWebCmcom() {
		return categoriaWebCmcom;
	}

	public void setCategoriaWebCmcom(String categoriaWebCmcom) {
		this.categoriaWebCmcom = categoriaWebCmcom;
	}
}
