package com.carsa.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TIPO_DOCUMENTO")
public class TipoDocumento {

	public static final TipoDocumento DNI = new TipoDocumento(96);
	public static final TipoDocumento CUIT = new TipoDocumento(80);
	
	@Id
	private Long idTipoDocumento;
	@Column(name = "DESCRIP_PARA_EPSON")
	private String descripcionEpson;
	@Column(name = "DESCRIP_PARA_HASAR")
	private String descripcionHasar;
	@Column(name = "PERSONA_FISICA")
	private boolean paraPersonaFisica;
	
	public TipoDocumento() {
	}
	
	public TipoDocumento(long id) {
		this.idTipoDocumento = id;
	}

	public String getDescripcionEpson() {
		return descripcionEpson;
	}

	public void setDescripcionEpson(String descripcionEpson) {
		this.descripcionEpson = descripcionEpson;
	}

	public String getDescripcionHasar() {
		return descripcionHasar;
	}

	public void setDescripcionHasar(String descripcionHasar) {
		this.descripcionHasar = descripcionHasar;
	}

	public boolean isParaPersonaFisica() {
		return paraPersonaFisica;
	}

	public void setParaPersonaFisica(boolean paraPersonaFisica) {
		this.paraPersonaFisica = paraPersonaFisica;
	}

	public Long getIdTipoDocumento() {
		return idTipoDocumento;
	}
}
