package com.carsa.repository;

import org.springframework.stereotype.Repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import com.carsa.model.Articulo;


@Repository
public interface ArticuloRepository extends JpaRepository<Articulo,Long>{
	
	Page<Articulo> findAll(Pageable pageable);
	Optional<Articulo> findById(Long id);
}
