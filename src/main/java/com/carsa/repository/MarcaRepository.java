package com.carsa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.carsa.model.Marca;

@Repository
public interface MarcaRepository extends JpaRepository<Marca,Long>{

}
