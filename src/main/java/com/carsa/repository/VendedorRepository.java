package com.carsa.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.carsa.model.Vendedor;

public interface VendedorRepository extends JpaRepository<Vendedor,Long>{
	
	List<Vendedor> findByNombreLikeAndFechaBajaNull(String nombre);

}
