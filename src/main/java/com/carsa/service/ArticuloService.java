package com.carsa.service;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.carsa.model.Articulo;

public interface ArticuloService {
	
	Page<Articulo> getAll(Pageable pageable);
	Optional<Articulo> getArticulo(Long id);
	
}
