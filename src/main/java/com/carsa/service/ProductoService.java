package com.carsa.service;

import java.util.Optional;

import org.springframework.http.ResponseEntity;
import com.carsa.model.Producto;

public interface ProductoService {
	
	Optional<Producto> getProducto(Long id);

}
