package com.carsa.service;

import java.util.List;

import com.carsa.model.Vendedor;


public interface VendedorService {
	List<Vendedor> findAllByNombre(String nombre);
	Vendedor save(Vendedor vendedor);
	boolean add(Vendedor vendedor) throws Exception;
	boolean delete(Long id) throws Exception;
}
