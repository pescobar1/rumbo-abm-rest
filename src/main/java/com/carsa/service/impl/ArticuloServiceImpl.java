package com.carsa.service.impl;


import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.carsa.model.Articulo;
import com.carsa.repository.ArticuloRepository;
import com.carsa.service.ArticuloService;

@Service
public class ArticuloServiceImpl implements ArticuloService {
	
	@Autowired
	private ArticuloRepository articuloRepository;
	
	@Override
	public Optional <Articulo> getArticulo(Long id) {
		Optional<Articulo> articulo = articuloRepository.findById(id);
		return articulo;
	}

	@Override
	public Page<Articulo> getAll(Pageable pageable) {
		Page<Articulo> articulos = articuloRepository.findAll(pageable);
		return articulos;
	}
}

