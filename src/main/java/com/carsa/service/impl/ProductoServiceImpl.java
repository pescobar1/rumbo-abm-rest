package com.carsa.service.impl;


import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.carsa.model.Producto;
import com.carsa.repository.ProductoRepository;
import com.carsa.service.ProductoService;

@Service
public class ProductoServiceImpl implements ProductoService {
	
	@Autowired
	private ProductoRepository productoRepository;
	
	@Override
	public Optional <Producto> getProducto(Long id) {
		Optional<Producto> producto = productoRepository.findById(id);
		return producto;
	}
}
