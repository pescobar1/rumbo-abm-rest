package com.carsa.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.carsa.model.Vendedor;
import com.carsa.repository.VendedorRepository;
import com.carsa.service.VendedorService;

@Service
public class VendedorServiceImpl implements VendedorService {
	
	@Autowired
	VendedorRepository vendedorRepository;
	
	@Override
	public List<Vendedor> findAllByNombre(String nombre) {
		return vendedorRepository.findByNombreLikeAndFechaBajaNull(nombre);
	}
	
	@Override
	public Vendedor save(Vendedor vendedor) {
		vendedor.setFechaModificacion(new Date());
		return vendedorRepository.save(vendedor);
	}
	
	@Override
	public boolean add(Vendedor vendedor) throws Exception {
		try{
			vendedor.setFechaAlta(new Date());
			vendedorRepository.save(vendedor);
			System.out.println("VENDEDOR REGISTRADO CORRECTAMENTE");
			return true;
		}catch (Exception e) {
			System.out.println("OCURRIO UN ERROR DURANTE EL PROCESO" + e.getMessage());
			e.printStackTrace();
			throw new Exception("No se pudo Guardar el vendedor " + vendedor.getIdVendedor());
		}
		
	}
	
	@Override
	public boolean delete(Long id) throws Exception {
		try {
			Vendedor vendedor = vendedorRepository.findById(id).get();
			vendedor.setFechaBaja(new Date());
				this.save(vendedor);
			return true;
		} catch(Exception e) {
			e.printStackTrace();
			throw new Exception("No se pudo dar de baja el Vendedor "+ id);
			
		}	

	}
}
